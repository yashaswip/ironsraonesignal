﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdvertiseController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        IronSource.Agent.init("adb5dbc5");
    }

    public void BannerAdsMethods()
    {
        IronSource.Agent.loadBanner(IronSourceBannerSize.SMART, IronSourceBannerPosition.BOTTOM);
    }

    public void InterstitialMethod()
    {
       if(IronSource.Agent.isInterstitialReady())
       {
            IronSource.Agent.showInterstitial();
       }
       else
       {
          IronSource.Agent.loadInterstitial();

       }
    }

    public void RewardingAdsMethod()
    {
        if(IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo();
        }            
    }

}
